FROM python:3.9

EXPOSE 8092
RUN apt-get -y update \
    && apt-get install -y ffmpeg  \
    && rm -rf /var/lib/apt/lists/*
RUN mkdir /srv/data

ENV REDIS_HOST=""
ENV REDIS_CHANNEL=""
ENV TELEGTAM_TOKEN=""
ENV URL2CONTENT=""

ADD app /srv
COPY app/ /srv/app/
# ADD main.py /srv 
ADD mainf.py /srv

ADD requirements.txt /srv

WORKDIR /srv
RUN pip install -r requirements.txt


# CMD ["python", "/srv/main.py"]
CMD ["uvicorn", "mainf:app", "--host", "0.0.0.0", "--port", "8092"]