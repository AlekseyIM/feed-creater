import os


class Conf():
    def __init__(self):
        self.REDIS_HOST = os.environ.get('REDIS_HOST', '127.0.0.1')
        self.REDIS_CHANNEL = os.environ.get('REDIS_CHANNEL', "a3by2d")
        self.TELEGTAM_TOKEN = os.environ.get(
            'TELEGTAM_TOKEN', "")  # только для тестирования
        self.PATH2CONTENT = "data"
        self.URL2CONTENT = os.environ.get('URL2CONTENT', 'https://localhost/f/')

class TestConf():
    def __init__(self):
        self.REDIS_HOST = '127.0.0.1'
        self.REDIS_CHANNEL = "test"
        self.TELEGTAM_TOKEN =""
        self.PATH2CONTENT ='test_data'
        self.URL2CONTENT = 'https://localhost/f/'


test_contract_for_download = {
    # "url": 'https://www.youtube.com/watch?v=BaW_jenozKc',
    'url': 'https://www.youtube.com/watch?v=qhs5DTaOzHU&t',
    "add2feed": True,
    "send2telegram": False,
    'log2telegram': False,
    # https://github.com/yt-dlp/yt-dlp#format-selection-examples
    "format": 'ba[ext=m4a]/ba[ext=mp3]/ba[ext=aac]',
    # 'format': "(bv*[vcodec~='^((he|a)vc|h26[45])']+ba) / (bv*+ba/b)",
    "options": {
        'bot_options': {
            'chat_id': "",
            'token':  os.environ.get('TELEGTAM_TOKEN'),
            'reply2message': '',
        },
        'feed_options': {
            'name': 'rss',
            'description': "Мой личный фид",
        },
    }
}

test_info_cr_rss = {
    'filepath': 'data/220416yzqpop.webm',  # адресс медиа
    'fc_filename': '220416yzqpop.webm',
    'bot_options': {
        'chat_id': "304464612",
        'token': Conf().TELEGTAM_TOKEN,
    },
    'feed_options': {
        'name': 'rss',
        'description': "Мой личный фид",
    },
    'title': 'youtube-dl test video "\'/\\ä↭𝕐',
    'id': 'BaW_jenozKc',
    'thumbnail': 'https://i.ytimg.com/vi/BaW_jenozKc/maxresdefault.jpg',
    'description': 'test chars:  "\'/\\ä↭𝕐\ntest URL: https://github.com/rg3/youtube-dl/issues/1892\n\nThis is a test video for youtube-dl.\n\nFor more information, contact phihag@phihag.de .',
    'duration': 10,
    'webpage_url': 'https://www.youtube.com/watch?v=BaW_jenozKc',
    'upload_date': '20121002',
    'filesize': 142292,
    'ext': 'webm',
    'audio_ext': 'webm',
    'video_ext': 'none',
    'format': '251 - audio only (medium)'
}
