import xml.etree.ElementTree as ET
import os.path
from app.sen2telegram import send2telegramm
from datetime import datetime
from app.my_logger import Logger
from app.config import Conf
from urllib.parse import urljoin


class FeedCreater:
    def __init__(self, d: dict,  logger: Logger, conf: Conf):
        self.d = d
        self.logger = logger
        self.conf = conf
        self.path2rss = os.path.join(
            self.conf.PATH2CONTENT, self.d['options']['feed_options']['name'] + ".xml")

    def _feed_not_exist(self) -> bool:
        if not os.path.isdir(self.conf.PATH2CONTENT):
            os.makedirs(self.conf.PATH2CONTENT)
            return True

        if not os.path.isfile(self.path2rss):
            return True
        return False

    def _create_new_feed(self):

        rss = ET.Element("rss",  version="2.0")
        channel = ET.SubElement(rss, "channel")
        ET.SubElement(
            channel, "title").text = self.d['options']['feed_options']['name']
        ET.SubElement(channel, "link").text = urljoin(
            self.conf.URL2CONTENT, self.d['options']['feed_options']['name'] + ".xml")
        ET.SubElement(channel, "language").text = 'ru'
        ET.SubElement(channel, "copyright").text = 'BSD'
        # ET.SubElement(channel, "author").text = i['a3b_info']['rss_email']
        ET.SubElement(
            channel, "description").text = self.d['options']['feed_options']['description']
        # ET.SubElement(channel, "thumbnail").text = i['a3b_info']['rss_thumbnail']
        # ET.SubElement(channel, "credit", role="author").text = i['a3b_info']['user']
        ET.SubElement(channel, "rating").text = 'nonadult'

        tree = ET.ElementTree(rss)
        ET.indent(tree, space="\t", level=0)

        tree.write(self.path2rss, xml_declaration=True, encoding="utf-8", )

    def add2rss(self, info: dict):
        if self._feed_not_exist():
            self._create_new_feed()
        
        parser = ET.XMLParser(encoding="utf-8")
        tree = ET.parse(self.path2rss, parser=parser)
        root = tree.getroot()
        channel = root.find("channel")

        item = ET.Element("item")
        ET.SubElement(
            item, "title").text = info['title']
        ET.SubElement(item, "description").text = FeedCreater._escape(
            info["description"])
        # ET.SubElement(item, "summary").text = _escape(i["description"])
        # ET.SubElement(item, "image").text = i['thumbnail']
        ET.SubElement(item, "link").text = info['webpage_url']
        ET.SubElement(item, "guid").text = info['webpage_url']
        # ET.SubElement(item, "author").text = i['a3b_info']['user']
        ET.SubElement(item, "pubDate").text = datetime.now().strftime(
            "%a, %d %b %Y %H:%M:%S") + " +0300"
        # <pubDate>Tue, 02 Oct 2016 19:45:02</pubDate>

        url2media = urljoin(
            self.conf.URL2CONTENT,
            self.d['options']['feed_options']['name'])
        url2media = urljoin(
            url2media, self.d['options']['feed_options']['name'] + '/')
        url2media = urljoin(url2media, self.d['filename'] + '.' + info['ext'])

        media_type = 'audio/' + info["ext"]
        # content = ET.SubElement(item, "content")
        # content.set("url", url2media)
        # content.set("fileSize", str(i["filesize"]))
        # content.set("type", media_type)

        enclosure = ET.SubElement(item, "enclosure")
        enclosure.set("url", url2media)

        fileSize = str(info.get('filesize', ""))
        if fileSize != "":
            enclosure.set("length", fileSize)
        enclosure.set("type", media_type)

        channel.append(item)

        ET.indent(tree, space="\t", level=0)
        tree.write(self.path2rss, xml_declaration=True, encoding="utf-8")
        msg = "Добавлено в feed: {}".format(self.d['options']['feed_options']['name'])
        self.logger.log2telegram(msg)

    def _escape(text: str):
        text = text.replace("&", "&amp;")
        text = text.replace("<", "&lt;")
        text = text.replace(">", "&gt;")
        text = text.replace("\"", "&quot;")
        return text
