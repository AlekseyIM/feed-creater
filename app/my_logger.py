from typing import Callable


class Logger:
    def __init__(self, log2t: bool = False, sender: Callable = None, token: str = None, chat_id: str = None, reply2message: int = None):
        self.sender = sender
        self.token = token
        self.chat_id = chat_id
        self.reply2message = reply2message
        self.log2t = log2t

    def debug(self, msg):
        # For compatibility with youtube-dl, both debug and info are passed into debug
        # You can distinguish them by the prefix '[debug] '
        if msg.startswith('[debug] '):
            pass
        else:
            self.info(msg)

    def info(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)

    def log2telegram(self, msg: str):
        if not self.log2t or self.sender is None or self.token is None or self.chat_id is None:
            print("Not send telegram", msg)
            return
        print("Send telegram", msg)
        self.sender(self.token, self.chat_id, msg, self.reply2message)

class TestLogger:
    def debug(self, msg):
    # For compatibility with youtube-dl, both debug and info are passed into debug
    # You can distinguish them by the prefix '[debug] '
        pass

    def info(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        pass

    def log2telegram(self, msg: str):
        pass

