import requests

# https://telegram-bot-sdk.readme.io/reference/sendmessage

def send2telegramm(token: str, chat_id: str, msg: str, reply2message: int=None):
    url = "https://api.telegram.org/bot"+ token + "/sendMessage"

    payload = {
        "text": msg,
        "parse_mode": "Markdown",
        "disable_web_page_preview": False,
        "disable_notification": False,
        "reply_to_message_id": reply2message,
        "chat_id": chat_id
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
    }
    response = requests.request("POST", url, json=payload, headers=headers)
    print(response.text)

def send_audio2telegram(token: str, chat_id: str, audio: str, reply2message: int=None):
    url = "https://api.telegram.org/bot"+ token + "/sendMessage"

    payload = {
        "audio": audio,
        "parse_mode": "Markdown",
        "disable_web_page_preview": False,
        "disable_notification": False,
        "reply_to_message_id": reply2message,
        "chat_id": chat_id
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
    }
    
    response = requests.request("POST", url, json=payload, headers=headers)
    print(response)

if __name__ == '__main__':
    # https://h.a3b.me/f/220412djxhdi.m4a
    from app.config import data_for_d
    
    conf = data_for_d['extra_info']['a3b_info']
    print(conf)
    send2telegramm(conf['bot-token'], conf['chat_id'],"пошла")
    # send_audio2telegram(conf['bot-token'], conf['chat_id'], "https://h.a3b.me/f/220416jbpwtn.m4a")