#!/usr/bin/env python
import json
import yt_dlp
import functools
import string
import random
from datetime import date
import os
from app.my_logger import Logger
from app.config import Conf
from app.cr_rss import FeedCreater
# from frss import add2rss
# from t import send2telegramm


class PPadd2feed(yt_dlp.postprocessor.PostProcessor):
    def run(self, info):
        self.fc.add2rss(info)
        return [], info


class PPsend2telegram(yt_dlp.postprocessor.PostProcessor):
    pass


def download(d: dict, conf: Conf, logger: Logger, fc: FeedCreater = None):

    filename = _generate_file_name()
    filepath = str(os.path.join(conf.PATH2CONTENT,
                   d['options']['feed_options']['name'], filename)) + '.%(ext)s'

    d['filename'] = filename

    logger.log2telegram("Пробую загрузить для {}".format(
        d['options']['feed_options']['name']))

    ydl_opts = {
        'outtmpl': filepath,
        'format': d['format'],
        'logger': logger,
        'http_headers': {'Referer': 'https://www.google.com'}
    }

    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        if d['add2feed']:
            ppadd2feed = PPadd2feed()
            ppadd2feed.fc = fc
            ydl.add_post_processor(ppadd2feed)
        if d['send2telegram']:
            ydl.add_post_processor(PPsend2telegram())

        ydl.download(d["url"])
        # try:
        #     ydl.download(d["url"])
        # # info = ydl.extract_info(d['url'], download=True)
        # except Exception as e:
        #     msg = "Неудалось загрузить файл: " + str(e)
        #     logger.log2telegram(
        #             token=d['options']['bot_options']['token'],
        #             chat_id=d['options']['bot_options']['chat_id'],
        #             msg=msg,
        #             reply2message=d['options']['bot_options'].get("reply2message", None)
        # )


def _generate_file_name():
    fname = date.today().strftime("%y%m%d")
    fname = fname + "".join(random.choice(string.ascii_lowercase)
                            for _ in range(6))
    return fname


if __name__ == '__main__':
    from app.config import test_contract_for_download
    c = Conf()
    l = Logger()
    fc = FeedCreater(l, c)
    print("start")
    download(
        d=test_contract_for_download,
        conf=c,
        logger=l,
        fc=fc
    )
