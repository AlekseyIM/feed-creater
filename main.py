#!/usr/bin/python
import redis
import json
from multiprocessing import Process
import time
from app.ydtlp import download
from app.config import Conf
from app.my_logger import Logger
from app.sen2telegram import send2telegramm
from app.cr_rss import FeedCreater
import os


def start_download(data: dict):
    print("try to download")
    l = Logger(log2t=data['log2telegram'],
           sender=send2telegramm,
           token=data["options"]['bot_options']['token'],
           chat_id=data["options"]['bot_options']['chat_id'],
           reply2message=data["options"]['bot_options']['reply2message'])
    c = Conf()
    fc = FeedCreater(data, l, c)
    download(data, Conf(), l, fc)
    print("Download complited")


if __name__ == '__main__':
    print("start app")
    conf = Conf()
    print("redis host: {}, url: {}".format(conf.REDIS_HOST, conf.URL2CONTENT))

    # init content path
    if not os.path.isdir(conf.PATH2CONTENT):
         os.makedirs(conf.PATH2CONTENT)
    # init redis
    r = redis.Redis(host=conf.REDIS_HOST)
    p = r.pubsub()
    p.subscribe(conf.REDIS_CHANNEL)

    print("Start to lisen")
    while True:
        try:
            message = p.get_message()
        except redis.ConnectionError:
            # Do reconnection attempts here such as sleeping and retrying
            print("reconnect to redis after 3 sec")
            time.sleep(3)
            p = r.pubsub()
            p.subscribe(conf.REDIS_CHANNEL)
        if message:
            if message["type"] == "message":
                m = json.loads(message["data"])
                # start download in background
                proccess = Process(target=start_download, args=(m,))
                proccess.start()
        time.sleep(1)  # be nice to the system :)
