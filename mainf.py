#!/usr/bin/python
import time
from app.ydtlp import download
from app.config import Conf
from app.my_logger import Logger
from app.sen2telegram import send2telegramm
from app.cr_rss import FeedCreater
import os
import uvicorn
from fastapi import FastAPI, BackgroundTasks
from pydantic import BaseModel
from typing import Optional

class BotOptions(BaseModel):
    chat_id: str
    token: str
    reply2message: str

class FeedOptions(BaseModel):
    name: str
    description: str

class Options(BaseModel):
    bot_options: BotOptions
    feed_options: FeedOptions

class DownloadData(BaseModel):
    url: str
    add2feed: bool
    send2telegram: bool
    log2telegram: bool
    format: str
    options: Options


class Ydres(BaseModel):
    status:str
    error: Optional[str] = None


def start_download(data: dict):
    print("try to download")
    l = Logger(log2t=data['log2telegram'],
           sender=send2telegramm,
           token=data["options"]['bot_options']['token'],
           chat_id=data["options"]['bot_options']['chat_id'],
           reply2message=data["options"]['bot_options']['reply2message'])
    c = Conf()
    fc = FeedCreater(data, l, c)
    download(data, Conf(), l, fc)
    print("Download complited")


print("start app")
conf = Conf()
print("host:port TODO")

# init content path
if not os.path.isdir(conf.PATH2CONTENT):
        os.makedirs(conf.PATH2CONTENT)

app = FastAPI()

@app.post("/d/")
async def create_item(req: DownloadData, background_tasks: BackgroundTasks):
    error = None
    try:
        print("1")
        background_tasks.add_task(start_download, req.dict())
    except ValueError:
        error = ValueError

    if error:
        return Ydres(status="not Ok", error=error)
    
    return Ydres(status="Ok", error="")

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)