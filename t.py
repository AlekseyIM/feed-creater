from math import log2
from app.config import Conf
from app.my_logger import Logger
from app.cr_rss import FeedCreater
from app.ydtlp import download
from app.sen2telegram import send2telegramm
import os

test_contract_for_download = {
    # "url": 'https://www.youtube.com/watch?v=BaW_jenozKc',
    'url': 'https://www.youtube.com/watch?v=qhs5DTaOzHU&t',
    "add2feed": True,
    "send2telegram": False,
    'log2telegram': False,
    # https://github.com/yt-dlp/yt-dlp#format-selection-examples
    "format": 'ba[ext=m4a]/ba[ext=mp3]/ba[ext=aac]',
    # 'format': "(bv*[vcodec~='^((he|a)vc|h26[45])']+ba) / (bv*+ba/b)",
    "options": {
        'bot_options': {
            'chat_id': "304464612",
            'token':  os.environ.get('TELEGTAM_TOKEN'),
            'reply2message': '122',
        },
        'feed_options': {
            'name': 'rss',
            'description': "Мой личный фид",
        },
    }
}

c = Conf()
l = Logger(log2t=test_contract_for_download['log2telegram'],
           sender=send2telegramm,
           token=test_contract_for_download["options"]['bot_options']['token'],
           chat_id=test_contract_for_download["options"]['bot_options']['chat_id'],
           reply2message=test_contract_for_download["options"]['bot_options']['reply2message'])
fc = FeedCreater(test_contract_for_download, l, c)
print("start")
download(
    d=test_contract_for_download,
    conf=c,
    logger=l,
    fc=fc)
