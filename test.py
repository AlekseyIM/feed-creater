import unittest
from tests.test_cr_rss import TestCrRss
from tests.test_config import TestConf
from tests.test_ydtlp import TestYdtlp


def suite():
    suite = unittest.TestSuite()
    
    suite.addTest(TestCrRss('test_create_new_rss'))
    suite.addTest(TestCrRss("test_add2rss"))

    suite.addTest(TestConf('test_init_difault_conf'))

    suite.addTest(TestYdtlp('test_generate_file_name'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
