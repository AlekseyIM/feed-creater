import unittest
from app.config import Conf
import os


class TestConf(unittest.TestCase):
    def setUp(self):
        self.REDIS_HOST = os.environ.get('REDIS_HOST', '127.0.0.1')
        self.REDIS_CHANNEL = os.environ.get('REDIS_CHANNEL', "a3by2d")
        self.TELEGTAM_TOKEN = os.environ.get(
            'TELEGTAM_TOKEN', "")
        self.PATH2CONTENT = os.environ.get('PATH2CONTENT', 'data')
        self.URL2CONTENT = os.environ.get('URL2CONTENT', 'https://localhost/f/')

    def test_init_difault_conf(self):
        conf = Conf()
        self.assertEqual(self.REDIS_HOST, conf.REDIS_HOST,
                         "REDIS_HOST not init")
        self.assertEqual(self.REDIS_CHANNEL, conf.REDIS_CHANNEL,
                         "REDIS_CHANNEL not init")
        self.assertEqual(self.TELEGTAM_TOKEN,
                         conf.TELEGTAM_TOKEN, "TELEGTAM_TOKEN not init")

        self.assertEqual(self.PATH2CONTENT, conf.PATH2CONTENT, "PATH2CONTENT")
        self.assertEqual(self.URL2CONTENT, conf.URL2CONTENT, 'URL2CONTENT')

