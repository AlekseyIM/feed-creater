import unittest
from app.cr_rss import FeedCreater
from app.my_logger import TestLogger
from app.config import TestConf, test_info_cr_rss, test_info_cr_rss, test_contract_for_download
import os


class TestCrRss(unittest.TestCase):

    def setUp(self):
        self.l = TestLogger()
        self.c = TestConf()
        self.path = os.path.join(
            self.c.PATH2CONTENT, test_contract_for_download['options']['feed_options']['name'] + ".xml")
        if os.path.exists(self.path):
            os.remove(self.path)

    def test_create_new_rss(self):
        fc = FeedCreater(test_contract_for_download, self.l, self.c)
        self.assertTrue(fc._feed_not_exist(),
                        "Ошибка проверки существования feedа")
        fc._create_new_feed()
        self.assertTrue(os.path.exists(self.path))
        # TODO: проверить качество фида
    
    def test_add2rss(self):
        test_contract_for_download['filename'] = 'somefile'
        fc = FeedCreater(test_contract_for_download, self.l, self.c)
        fc.add2rss(test_info_cr_rss)
        # TODO: написать тест

if __name__ == '__main__':
    unittest.main()
