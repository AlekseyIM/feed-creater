import unittest
import app.ydtlp as d
from app.cr_rss import FeedCreater
from app.my_logger import TestLogger
from app.config import TestConf, test_info_cr_rss, test_info_cr_rss, test_contract_for_download
import os


class TestYdtlp(unittest.TestCase):

    def setUp(self):
        pass

    def test_generate_file_name(self):
        test = d._generate_file_name()
        test2 = d._generate_file_name()
        self.assertNotEqual(test,test2, "_generate_file_name is broken")
        self.assertNotEqual('', test)
        self.assertNotEqual('', test2)
        self.assertIsNotNone(test)
        self.assertIsNotNone(test2)